module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '~cs62160239/learn_bootstrap/'
    : '/'
}